﻿namespace IOTLinkAddon.Common
{
    public enum AddonRequestType
    {
        REQUEST_DISPLAY_TURN_ON,
        REQUEST_DISPLAY_TURN_OFF
    }
}
